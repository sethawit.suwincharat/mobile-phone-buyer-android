package com.full.mobile.ui.mobiledetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.full.mobile.data.local.db.entity.MobileImageEntity
import com.full.mobile.data.remote.base.ApiResponse
import com.full.mobile.domain.usecase.GetMobileImageListUseCase
import com.full.mobile.ui.base.BaseViewModel
import com.full.mobile.utlis.SingleLiveEvent
import kotlinx.coroutines.launch

class MobileDetailViewModel(
    private val getMobileImageListUseCase: GetMobileImageListUseCase
) : BaseViewModel() {
    val networkError = SingleLiveEvent<Unit>()

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _error = MutableLiveData<Exception>()
    val error: LiveData<Exception> = _error

    private val _mobileImages = MutableLiveData<List<MobileImageEntity>>()
    val mobileImages: LiveData<List<MobileImageEntity>> = _mobileImages

    fun getMobileImages(mobileId: Int) {
        viewModelScope.launch {
            _isLoading.value = true
            when (val result = getMobileImageListUseCase.execute(mobileId)) {
                is ApiResponse.Success -> {
                    _isLoading.value = false
                    _mobileImages.value = result.data as List<MobileImageEntity>
                }
                is ApiResponse.Error -> {
                    _isLoading.value = false
                    _error.value = result.apiException
                }
            }
        }
    }
}
