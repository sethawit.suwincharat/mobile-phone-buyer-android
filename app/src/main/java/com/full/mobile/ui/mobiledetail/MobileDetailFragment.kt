package com.full.mobile.ui.mobiledetail

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.full.mobile.Constants.EMPTY_STRING
import com.full.mobile.R
import com.full.mobile.data.local.db.entity.MobileImageEntity
import com.full.mobile.databinding.FragmentMobileDetailBinding
import com.full.mobile.domain.models.Mobile
import com.full.mobile.ui.base.BaseFragment
import com.full.mobile.ui.mobilelist.MobileListFragment
import com.full.mobile.utlis.showSnackBar
import com.full.mobile.utlis.showSnackBarWithAction
import org.koin.androidx.viewmodel.ext.android.viewModel


class MobileDetailFragment : BaseFragment<FragmentMobileDetailBinding>() {
    private val viewModel: MobileDetailViewModel by viewModel()
    private val args: MobileDetailFragmentArgs by navArgs()
    private var mobile: Mobile? = null
    private var sourceTag = EMPTY_STRING

    private val imageAdapter by lazy {
        ImageAdapter()
    }

    override fun layout() = R.layout.fragment_mobile_detail

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mobile = args.mobile
        sourceTag = args.tag

        initObservers()
        initViews()
        bindEvents()

        if (mobile != null) mobile?.id?.let {
            viewModel.getMobileImages(it)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                findNavController().popBackStack()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initObservers() {
        viewModel.isLoading.observe(viewLifecycleOwner) {
            handleLoading(it)
        }
        viewModel.mobileImages.observe(viewLifecycleOwner) {
            renderMobileDetail(it)
        }
        viewModel.error.observe(viewLifecycleOwner) {
            showSnackBar(it?.message ?: getString(R.string.error_message_something_went_wrong))
        }
        viewModel.networkError.observe(viewLifecycleOwner) {
            showSnackBarWithAction(
                getString(R.string.error_message_no_internet),
                getString(R.string.common_retry)
            ) {
                if (mobile != null) mobile?.id?.let {
                    viewModel.getMobileImages(it)
                }
            }
        }
    }

    private fun initViews() {
        (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)

        when (sourceTag) {
            MobileListFragment.TAG -> binding.toolbar.title = getString(R.string.mobile_list)
            else -> binding.toolbar.title = getString(R.string.favourite_list)
        }

        binding.imageViewPager.adapter = imageAdapter
    }

    private fun bindEvents() {
        binding.refreshLayout.setOnRefreshListener {
            if (mobile != null) mobile?.id?.let {
                viewModel.getMobileImages(it)
            }
            binding.refreshLayout.isRefreshing = false
        }
    }

    private fun handleLoading(loading: Boolean) {
        with(binding) {
            refreshLayout.isRefreshing = loading == true
        }
    }

    private fun renderMobileDetail(images: List<MobileImageEntity>) {
        if (images.isEmpty()) {
            renderDetail()
            showSnackBar(getString(R.string.error_message_no_images_found))
        } else {
            imageAdapter.updateData(images.map {
                it.url
            })
            renderDetail()
        }
    }

    private fun renderDetail() {
        with(binding) {
            ratingText.text = getString(R.string.rating, mobile?.rating.toString())
            priceText.text = getString(R.string.price, mobile?.price.toString())
            title.text = mobile?.name
            descriptionText.text = mobile?.description
        }
    }
}
