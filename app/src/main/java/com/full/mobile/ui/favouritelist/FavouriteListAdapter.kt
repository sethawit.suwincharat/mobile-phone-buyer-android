package com.full.mobile.ui.favouritelist

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.full.mobile.R
import com.full.mobile.databinding.ItemMobileBinding
import com.full.mobile.domain.models.Mobile
import com.full.mobile.utlis.bindImageFromUrl

class FavouriteListAdapter(
    private val mobileCardClicked: (mobile: Mobile) -> Unit
) : RecyclerView.Adapter<FavouriteListAdapter.ViewHolder>() {
    private var items = mutableListOf<Mobile>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemMobileBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    fun updateData(items: MutableList<Mobile>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun removeAt(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    fun getItemAt(position: Int): Mobile {
        return items[position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(private val binding: ItemMobileBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val context: Context = binding.root.context
        fun bind(item: Mobile) {
            with(binding) {
                favouriteIcon.isVisible = false
                bindImageFromUrl(mobileImage, item.thumbImageURL)
                mobileTitle.text = item.name
                mobileDescription.text = item.description
                mobilePrice.text = context.getString(R.string.price, item.price.toString())
                mobileRating.text = context.getString(R.string.rating, item.rating.toString())
                root.setOnClickListener {
                    mobileCardClicked.invoke(item)
                }
            }
        }
    }

    override fun getItemCount() = items.size
}
