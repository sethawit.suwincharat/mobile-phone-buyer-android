package com.full.mobile.ui.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()
