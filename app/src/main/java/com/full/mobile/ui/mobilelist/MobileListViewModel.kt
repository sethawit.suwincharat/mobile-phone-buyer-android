package com.full.mobile.ui.mobilelist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.full.mobile.data.remote.base.ApiResponse
import com.full.mobile.domain.models.Mobile
import com.full.mobile.domain.usecase.GetFavouriteListUseCase
import com.full.mobile.domain.usecase.GetMobileListUseCase
import com.full.mobile.domain.usecase.SetFavouriteUseCase
import com.full.mobile.ui.base.BaseViewModel
import com.full.mobile.utlis.SingleLiveEvent
import kotlinx.coroutines.launch

class MobileListViewModel(
    private val getMobileListUseCase: GetMobileListUseCase,
    private val setFavouriteUseCase: SetFavouriteUseCase,
    private val getFavouriteListUseCase: GetFavouriteListUseCase
) : BaseViewModel() {
    val networkError = SingleLiveEvent<Unit>()
    val isUpdated = SingleLiveEvent<Unit>()

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _error = MutableLiveData<Exception>()
    val error: LiveData<Exception> = _error

    private val _mobileList = MutableLiveData<List<Mobile>>()
    val mobileList: LiveData<List<Mobile>> = _mobileList

    private val _favouriteList = MutableLiveData<List<Mobile>>()
    val favouriteList: LiveData<List<Mobile>> = _favouriteList

    fun getMobileList(sorted: Int) {
        viewModelScope.launch {
            _isLoading.value = true
            when (val result = getMobileListUseCase.execute(sorted)) {
                is ApiResponse.Success -> {
                    _isLoading.value = false
                    _mobileList.value = result.data as List<Mobile>
                }
                is ApiResponse.Error -> {
                    _isLoading.value = false
                    _error.value = result.apiException
                }
            }
        }
    }

    fun setFavourite(mobileId: Int) {
        viewModelScope.launch {
            setFavouriteUseCase.execute(mobileId)
        }
    }

    fun getFavouriteList(sorted: Int) {
        viewModelScope.launch {
            _isLoading.value = true
            val result = getFavouriteListUseCase.execute(sorted)
            _favouriteList.value = result
            _isLoading.value = false
        }
    }
}
