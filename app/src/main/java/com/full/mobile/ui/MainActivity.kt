package com.full.mobile.ui

import com.full.mobile.R
import com.full.mobile.databinding.ActivityMainBinding
import com.full.mobile.ui.base.BaseActivity

class MainActivity : BaseActivity<ActivityMainBinding>() {
    override fun getLayoutResId() = R.layout.activity_main
}
