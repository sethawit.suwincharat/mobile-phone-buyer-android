package com.full.mobile.ui.base

import android.content.Context
import android.content.ContextWrapper
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.full.mobile.Constants.EMPTY_STRING
import com.full.mobile.data.remote.StatusCode
import com.full.mobile.utlis.AppLocaleProvider.appLocale
import com.full.mobile.utlis.setAppLocale
import com.full.mobile.utlis.showSnackBar

abstract class BaseActivity<DB : ViewDataBinding> : AppCompatActivity() {

    protected lateinit var binding: DB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        binding = DataBindingUtil.setContentView(this, getLayoutResId())
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ContextWrapper(newBase.setAppLocale(appLocale)))
    }

    /**
     * Abstract method to set layout resource id to use in activity.
     * NOTE: To show the difference, Fragment base class uses other approach of pass the
     * layout id (through constructor). Both approaches can be used in Fragments and Activities
     * and it's up to developer to choose.
     */
    @LayoutRes
    abstract fun getLayoutResId(): Int

    protected fun showErrorMessage(
        errorMessage: String? = null,
        errorCode: Int? = StatusCode.INTERNAL_ERROR
    ) {
        showSnackBar(errorMessage ?: EMPTY_STRING)
    }
}
