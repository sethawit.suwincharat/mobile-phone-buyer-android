package com.full.mobile.ui.favouritelist

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.full.mobile.R
import com.full.mobile.data.local.manager.SharedPrefManager
import com.full.mobile.databinding.FragmentFavouriteListBinding
import com.full.mobile.domain.models.Mobile
import com.full.mobile.ui.base.BaseFragment
import com.full.mobile.ui.mobilelist.MobileListViewModel
import com.full.mobile.ui.mobilelist.SwipeToDeleteCallback
import com.full.mobile.utlis.showSnackBar
import com.full.mobile.utlis.showSnackBarWithAction
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class FavouriteListFragment : BaseFragment<FragmentFavouriteListBinding>() {

    private val viewModel: MobileListViewModel by sharedViewModel()
    private val pref: SharedPrefManager by inject()
    private var sorted: Int = 1

    private val favouriteListAdapter by lazy {
        FavouriteListAdapter { mobile ->
            val action =
                FavouriteListFragmentDirections.actionFavouriteListFragmentToMobileDetailFragment(
                    mobile,
                    TAG
                )
            NavHostFragment.findNavController(this).navigate(action)
        }
    }

    override fun layout() = R.layout.fragment_favourite_list

    companion object {
        val TAG: String = FavouriteListFragment::class.java.simpleName
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initViews()
        bindEvents()
    }

    override fun onResume() {
        super.onResume()
        sorted = pref.sorted
        viewModel.getFavouriteList(sorted)
    }

    private fun initObservers() {
        viewModel.isLoading.observe(viewLifecycleOwner) {
            handleLoading(it)
        }
        viewModel.favouriteList.observe(viewLifecycleOwner) {
            renderFavouriteList(it)
        }
        viewModel.isUpdated.observe(viewLifecycleOwner) {
            viewModel.getFavouriteList(sorted)
        }
        viewModel.error.observe(viewLifecycleOwner) {
            showSnackBar(it?.message ?: getString(R.string.error_message_something_went_wrong))
        }
        viewModel.networkError.observe(viewLifecycleOwner) {
            showSnackBarWithAction(
                getString(R.string.error_message_no_internet),
                getString(R.string.common_retry)
            ) {
                viewModel.getFavouriteList(sorted)
            }
        }
    }

    private fun initViews() {
        binding.recyclerViewFavouriteList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = favouriteListAdapter
        }
    }

    private fun bindEvents() {
        binding.refreshLayout.setOnRefreshListener {
            viewModel.getFavouriteList(sorted)
            binding.refreshLayout.isRefreshing = false
        }
    }

    private fun handleLoading(loading: Boolean) {
        with(binding) {
            refreshLayout.isRefreshing = loading == true
        }
    }

    private fun renderFavouriteList(it: List<Mobile>) {
        if (it.isEmpty()) {
            showSnackBar(getString(R.string.error_message_no_data_found))
        } else {
            favouriteListAdapter.updateData(it.toMutableList())

            val swipeToDeleteCallback = object : SwipeToDeleteCallback() {
                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    val position = viewHolder.bindingAdapterPosition
                    viewModel.setFavourite(favouriteListAdapter.getItemAt(position).id)
                    favouriteListAdapter.removeAt(position)
                }
            }

            val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
            itemTouchHelper.attachToRecyclerView(binding.recyclerViewFavouriteList)
        }
    }
}
