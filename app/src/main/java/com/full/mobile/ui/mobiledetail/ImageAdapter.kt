package com.full.mobile.ui.mobiledetail

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.full.mobile.databinding.ItemImageBinding
import com.full.mobile.utlis.bindImageFromUrl

class ImageAdapter : RecyclerView.Adapter<ImageAdapter.ViewHolder>() {
    private var items = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemImageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    fun updateData(items: List<String>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    inner class ViewHolder(private val binding: ItemImageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val context: Context = binding.root.context
        fun bind(imageUrl: String) {
            with(binding) {
                bindImageFromUrl(mobileImage, imageUrl)
            }
        }
    }
}
