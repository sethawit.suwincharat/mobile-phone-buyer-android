package com.full.mobile.ui.mobilelist

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.full.mobile.R
import com.full.mobile.data.local.manager.SharedPrefManager
import com.full.mobile.databinding.FragmentMobileListBinding
import com.full.mobile.domain.models.Mobile
import com.full.mobile.ui.base.BaseFragment
import com.full.mobile.utlis.showSnackBar
import com.full.mobile.utlis.showSnackBarWithAction
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class MobileListFragment : BaseFragment<FragmentMobileListBinding>() {
    private val viewModel: MobileListViewModel by sharedViewModel()
    private val pref: SharedPrefManager by inject()
    private var sorted: Int = 1

    private val mobileListAdapter by lazy {
        MobileListAdapter({ mobileId ->
            viewModel.setFavourite(mobileId)
        }, { mobile ->
            val action =
                MobileListFragmentDirections.actionMobileListFragmentToMobileDetailFragment(
                    mobile,
                    TAG
                )
            NavHostFragment.findNavController(this).navigate(action)
        })
    }

    override fun layout() = R.layout.fragment_mobile_list

    companion object {
        val TAG: String = MobileListFragment::class.java.simpleName
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initViews()
        bindEvents()
    }

    override fun onResume() {
        super.onResume()
        sorted = pref.sorted
        viewModel.getMobileList(sorted)
    }

    private fun initObservers() {
        viewModel.isLoading.observe(viewLifecycleOwner) {
            handleLoading(it)
        }
        viewModel.mobileList.observe(viewLifecycleOwner) {
            renderMobileList(it)
        }
        viewModel.isUpdated.observe(viewLifecycleOwner) {
            viewModel.getMobileList(sorted)
        }
        viewModel.error.observe(viewLifecycleOwner) {
            showSnackBar(it?.message ?: getString(R.string.error_message_something_went_wrong))
        }
        viewModel.networkError.observe(viewLifecycleOwner) {
            showSnackBarWithAction(
                getString(R.string.error_message_no_internet),
                getString(R.string.common_retry)
            ) {
                viewModel.getMobileList(sorted)
            }
        }
    }

    private fun initViews() {
        binding.recyclerViewMobileList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = mobileListAdapter
        }
    }

    private fun bindEvents() {
        binding.refreshLayout.setOnRefreshListener {
            viewModel.getMobileList(sorted)
            binding.refreshLayout.isRefreshing = false
        }
    }

    private fun handleLoading(loading: Boolean) {
        with(binding) {
            refreshLayout.isRefreshing = loading == true
        }
    }

    private fun renderMobileList(it: List<Mobile>) {
        if (it.isEmpty()) {
            showSnackBar(getString(R.string.error_message_no_data_found))
        } else {
            mobileListAdapter.submitList(it)
        }
    }
}
