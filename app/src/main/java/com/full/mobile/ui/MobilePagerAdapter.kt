package com.full.mobile.ui

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.full.mobile.ui.favouritelist.FavouriteListFragment
import com.full.mobile.ui.mobilelist.MobileListFragment

const val MOBILE_LIST_INDEX = 0
const val FAVOURITE_LIST_INDEX = 1

class MobilePagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    private val tabFragmentsCreators: Map<Int, () -> Fragment> = mapOf(
        MOBILE_LIST_INDEX to { MobileListFragment() },
        FAVOURITE_LIST_INDEX to { FavouriteListFragment() }
    )

    override fun getItemCount() = tabFragmentsCreators.size

    override fun createFragment(position: Int): Fragment {
        return tabFragmentsCreators[position]?.invoke() ?: throw IndexOutOfBoundsException()
    }

}
