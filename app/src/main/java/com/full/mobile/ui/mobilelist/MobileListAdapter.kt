package com.full.mobile.ui.mobilelist

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.full.mobile.R
import com.full.mobile.databinding.ItemMobileBinding
import com.full.mobile.domain.models.Mobile
import com.full.mobile.utlis.bindImageFromUrl

class MobileListAdapter(
    private val favouriteClicked: (mobileId: Int) -> Unit,
    private val mobileCardClicked: (mobile: Mobile) -> Unit
) : ListAdapter<Mobile, MobileListAdapter.ViewHolder>(DiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemMobileBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    inner class ViewHolder(private val binding: ItemMobileBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val context: Context = binding.root.context
        private var fav: Boolean = false
        fun bind(item: Mobile) {
            with(binding) {
                bindImageFromUrl(mobileImage, item.thumbImageURL)
                mobileTitle.text = item.name
                mobileDescription.text = item.description
                mobilePrice.text = context.getString(R.string.price, item.price.toString())
                mobileRating.text = context.getString(R.string.rating, item.rating.toString())
                // First time
                fav = item.isFavourite
                setFavouriteIcon(fav)
                favouriteIcon.setOnClickListener {
                    favouriteClicked.invoke(item.id)
                    // Toggle
                    fav = !fav
                    setFavouriteIcon(fav)
                }
                root.setOnClickListener {
                    mobileCardClicked.invoke(item)
                }
            }
        }

        private fun setFavouriteIcon(isFavourite: Boolean) {
            if (isFavourite) {
                binding.favouriteIcon.setImageResource(R.drawable.ic_baseline_favorite_24)
            } else {
                binding.favouriteIcon.setImageResource(R.drawable.ic_outline_favorite_border_24)
            }
        }
    }
}

object DiffCallback : DiffUtil.ItemCallback<Mobile>() {
    override fun areItemsTheSame(oldItem: Mobile, newItem: Mobile): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: Mobile,
        newItem: Mobile
    ): Boolean {
        return oldItem.id == newItem.id
    }
}

