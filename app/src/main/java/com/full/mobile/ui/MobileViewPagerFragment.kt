package com.full.mobile.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.full.mobile.R
import com.full.mobile.data.local.manager.SharedPrefManager
import com.full.mobile.databinding.FragmentMobileViewPagerBinding
import com.full.mobile.ui.base.BaseFragment
import com.full.mobile.ui.mobilelist.MobileListViewModel
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class MobileViewPagerFragment : BaseFragment<FragmentMobileViewPagerBinding>() {
    private val viewModel: MobileListViewModel by sharedViewModel()

    override fun layout(): Int = R.layout.fragment_mobile_view_pager
    private val pref: SharedPrefManager by inject()
    private var currentTab = MOBILE_LIST_INDEX

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pref.sorted = 1
        initView()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_item, menu)
        menu.findItem(R.id.low_to_high).isChecked = true

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.low_to_high -> {
                item.isChecked = pref.sorted == 1
                pref.sorted = 1
                reloadData()
                false
            }
            R.id.high_to_low -> {
                item.isChecked = pref.sorted == 2
                pref.sorted = 2
                reloadData()
                false
            }
            R.id.five_to_one -> {
                item.isChecked = pref.sorted == 3
                pref.sorted = 3
                reloadData()
                false
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun initView() {
        (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(true)
        setHasOptionsMenu(true)

        binding.viewPager.adapter = MobilePagerAdapter(this)
        TabLayoutMediator(binding.tabs, binding.viewPager) { tab, position ->
            tab.text = getTabTitle(position)
        }.attach()
        binding.tabs.getTabAt(MOBILE_LIST_INDEX)?.select()
        binding.tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                currentTab = tab!!.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })
    }

    private fun getTabTitle(position: Int): String? {
        return when (position) {
            MOBILE_LIST_INDEX -> getString(R.string.mobile_list)
            FAVOURITE_LIST_INDEX -> getString(R.string.favourite_list)
            else -> null
        }
    }

    private fun reloadData() {
        when (currentTab) {
            MOBILE_LIST_INDEX -> viewModel.getMobileList(pref.sorted)
            FAVOURITE_LIST_INDEX -> viewModel.getFavouriteList(pref.sorted)
        }
    }
}
