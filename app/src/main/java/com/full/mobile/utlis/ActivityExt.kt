package com.full.mobile.utlis

import android.app.Activity
import android.content.Context
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar

fun Activity.showKeyboard() {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
}

fun Activity.showSnackBar(message: String) {
    Snackbar.make(
        findViewById(android.R.id.content),
        message,
        Snackbar.LENGTH_SHORT
    ).show()
}

fun Activity.showSnackBarWithAction(message: String, actionName: String, action: () -> Unit) {
    Snackbar.make(
        findViewById(android.R.id.content),
        message,
        Snackbar.LENGTH_INDEFINITE
    ).setAction(actionName) {
        action()
    }.show()
}

fun Activity.showSnackBar(@StringRes messageId: Int) {
    Snackbar.make(
        findViewById(android.R.id.content),
        messageId,
        Snackbar.LENGTH_SHORT
    ).show()
}

