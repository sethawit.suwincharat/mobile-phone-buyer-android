package com.full.mobile.utlis

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

fun Fragment.showSnackBar(message: String) {
    requireActivity().showSnackBar(message)
}

fun Fragment.showSnackBarWithAction(message: String, actionName: String, action: () -> Unit) {
    requireActivity().showSnackBarWithAction(message, actionName, action)
}

fun Fragment.showSnackBar(@StringRes messageId: Int) {
    requireActivity().showSnackBar(messageId)
}

fun Fragment.showKeyboard() {
    requireActivity().showKeyboard()
}

