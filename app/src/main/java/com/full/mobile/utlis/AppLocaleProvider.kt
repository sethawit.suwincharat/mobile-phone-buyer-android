package com.full.mobile.utlis

import com.full.mobile.LocaleConstants.LOCALE_EN


/**
 * Additional object to manage selected app language.
 * Should be used only during common language switch implementation.
 * If Localize or other 3rd party lib is used - this file should be removed.
 */
object AppLocaleProvider {
    var appLocale: AppLocale = LOCALE_EN
        set(value) {
            if (value.isNotEmpty()) {
                field = value
            }
        }
}

typealias AppLocale = String
