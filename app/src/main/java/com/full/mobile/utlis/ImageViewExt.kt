package com.full.mobile.utlis

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.full.mobile.R

private const val WORLD_WIDE_WEB = "www."
private const val HTTP_WORLD_WIDE_WEB = "http://www."

@BindingAdapter("imageFromUrl")
fun bindImageFromUrl(view: ImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        var url = imageUrl
        if (url.startsWith(WORLD_WIDE_WEB)) {
            url = url.replace(WORLD_WIDE_WEB.toRegex(), HTTP_WORLD_WIDE_WEB)
        }
        Glide.with(view.context)
            .load(url)
            .error(R.drawable.ic_baseline_error_24)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .transition(DrawableTransitionOptions.withCrossFade())
            .fitCenter()
            .into(view)
    }
}
