package com.full.mobile.di

import androidx.room.Room
import com.full.mobile.data.local.db.MobileDatabase
import com.full.mobile.data.local.manager.DatabaseManager
import com.full.mobile.data.local.manager.LocalDataManager
import com.full.mobile.data.local.manager.SharedPrefManager
import com.full.mobile.data.local.manager.impl.DatabaseManagerImpl
import com.full.mobile.data.local.manager.impl.LocalDataManagerImpl
import com.full.mobile.data.local.manager.impl.SharedPrefManagerImpl
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.dsl.single

val dataModule = module {
    single {
        Room.databaseBuilder(
            get(),
            MobileDatabase::class.java,
            "MobileDatabase"
        ).build()
    }
    single<DatabaseManagerImpl>() bind DatabaseManager::class
    single<SharedPrefManagerImpl>() bind SharedPrefManager::class
    single<LocalDataManagerImpl>() bind LocalDataManager::class
}
