package com.full.mobile.di

import com.full.mobile.data.network.NetworkStateProvider
import com.full.mobile.data.network.NetworkStateProviderImpl
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.dsl.single

val appModule = module {
    single {
        Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }
    single<NetworkStateProviderImpl>() bind NetworkStateProvider::class
}
