package com.full.mobile.di

import com.full.mobile.ui.mobiledetail.MobileDetailViewModel
import com.full.mobile.ui.mobilelist.MobileListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel<MobileListViewModel>()
    viewModel<MobileDetailViewModel>()
}
