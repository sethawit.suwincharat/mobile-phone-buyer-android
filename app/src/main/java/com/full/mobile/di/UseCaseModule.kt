package com.full.mobile.di

import com.full.mobile.domain.usecase.GetFavouriteListUseCase
import com.full.mobile.domain.usecase.GetMobileImageListUseCase
import com.full.mobile.domain.usecase.GetMobileListUseCase
import com.full.mobile.domain.usecase.SetFavouriteUseCase
import com.full.mobile.domain.usecase.impl.GetFavouriteListUseCaseImpl
import com.full.mobile.domain.usecase.impl.GetMobileImageListUseCaseImpl
import com.full.mobile.domain.usecase.impl.GetMobileListUseCaseImpl
import com.full.mobile.domain.usecase.impl.SetFavouriteUseCaseImpl
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.dsl.single

val useCaseModule = module {
    single<GetMobileListUseCaseImpl>() bind GetMobileListUseCase::class
    single<SetFavouriteUseCaseImpl>() bind SetFavouriteUseCase::class
    single<GetFavouriteListUseCaseImpl>() bind GetFavouriteListUseCase::class
    single<GetMobileImageListUseCaseImpl>() bind GetMobileImageListUseCase::class
}
