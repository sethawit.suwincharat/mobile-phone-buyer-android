package com.full.mobile.di

import com.full.mobile.data.repository.MobilePhoneRepositoryImpl
import com.full.mobile.domain.repository.MobilePhoneRepository
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.dsl.single

val repositoryModule = module {
    single<MobilePhoneRepositoryImpl>() bind MobilePhoneRepository::class
}
