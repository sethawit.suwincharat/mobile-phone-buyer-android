package com.full.mobile.di

import com.full.mobile.BuildConfig
import com.full.mobile.data.remote.RetrofitProvider
import com.full.mobile.data.remote.service.MobilePhoneService
import org.koin.dsl.module
import retrofit2.Retrofit

val apiModule = module {
    single {
        RetrofitProvider(
            moshi = get(),
            networkStateProvider = get(),
            apiUrl = BuildConfig.BASE_URL,
        ).provide()
    }
    single {
        get<Retrofit>().create(MobilePhoneService::class.java)
    }
}
