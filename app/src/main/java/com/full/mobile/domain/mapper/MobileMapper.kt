package com.full.mobile.domain.mapper

import com.full.mobile.data.local.db.entity.MobilePhoneEntity
import com.full.mobile.domain.models.Mobile

fun MobilePhoneEntity.toModel() = Mobile(
    brand = brand,
    description = description,
    id = id,
    name = name,
    price = price,
    rating = rating,
    thumbImageURL = thumbImageURL,
    isFavourite = isFavourite
)
