package com.full.mobile.domain.models

import java.io.Serializable

data class Mobile(
    val brand: String,
    val description: String,
    val id: Int,
    val name: String,
    val price: Double,
    val rating: Double,
    val thumbImageURL: String,
    val isFavourite: Boolean
) : Serializable
