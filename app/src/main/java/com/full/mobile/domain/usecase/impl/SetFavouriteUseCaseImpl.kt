package com.full.mobile.domain.usecase.impl

import com.full.mobile.domain.repository.MobilePhoneRepository
import com.full.mobile.domain.usecase.SetFavouriteUseCase

class SetFavouriteUseCaseImpl(
    private val mobilePhoneRepository: MobilePhoneRepository
) : SetFavouriteUseCase {
    override suspend fun execute(mobileId: Int) {
        mobilePhoneRepository.setFavourite(mobileId)
    }
}
