package com.full.mobile.domain.usecase

import com.full.mobile.data.local.db.entity.MobileImageEntity
import com.full.mobile.data.remote.base.ApiResponse

interface GetMobileImageListUseCase {
    suspend fun execute(mobileId: Int): ApiResponse<List<MobileImageEntity>>
}
