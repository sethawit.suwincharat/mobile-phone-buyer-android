package com.full.mobile.domain.usecase.impl

import com.full.mobile.data.local.db.entity.MobileImageEntity
import com.full.mobile.data.remote.base.ApiResponse
import com.full.mobile.domain.repository.MobilePhoneRepository
import com.full.mobile.domain.usecase.GetMobileImageListUseCase

class GetMobileImageListUseCaseImpl(
    private val mobilePhoneRepository: MobilePhoneRepository
) : GetMobileImageListUseCase {
    override suspend fun execute(mobileId: Int): ApiResponse<List<MobileImageEntity>> {
        return mobilePhoneRepository.getMobileImages(mobileId)
    }
}
