package com.full.mobile.domain.usecase

import com.full.mobile.data.remote.base.ApiResponse
import com.full.mobile.domain.models.Mobile

interface GetMobileListUseCase {
    suspend fun execute(sorted: Int): ApiResponse<List<Mobile>>
}
