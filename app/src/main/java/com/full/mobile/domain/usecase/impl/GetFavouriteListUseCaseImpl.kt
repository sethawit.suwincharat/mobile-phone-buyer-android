package com.full.mobile.domain.usecase.impl

import com.full.mobile.domain.models.Mobile
import com.full.mobile.domain.repository.MobilePhoneRepository
import com.full.mobile.domain.usecase.GetFavouriteListUseCase

class GetFavouriteListUseCaseImpl(
    private val mobilePhoneRepository: MobilePhoneRepository
) : GetFavouriteListUseCase {
    override suspend fun execute(sorted: Int): List<Mobile> {
        return mobilePhoneRepository.getFavouriteList(sorted)
    }
}
