package com.full.mobile.domain.repository

import com.full.mobile.data.local.db.entity.MobileImageEntity
import com.full.mobile.data.remote.base.ApiResponse
import com.full.mobile.data.remote.base.BaseRepository
import com.full.mobile.domain.models.Mobile

interface MobilePhoneRepository : BaseRepository {
    suspend fun getMobileList(sorted: Int): ApiResponse<List<Mobile>>
    suspend fun setFavourite(mobileId: Int)
    suspend fun getFavouriteList(sorted: Int): List<Mobile>
    suspend fun getMobileImages(mobileId: Int): ApiResponse<List<MobileImageEntity>>
}
