package com.full.mobile.domain.usecase

interface SetFavouriteUseCase {
    suspend fun execute(mobileId: Int)
}
