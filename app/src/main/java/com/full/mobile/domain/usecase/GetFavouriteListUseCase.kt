package com.full.mobile.domain.usecase

import com.full.mobile.domain.models.Mobile

interface GetFavouriteListUseCase {
    suspend fun execute(sorted: Int): List<Mobile>
}
