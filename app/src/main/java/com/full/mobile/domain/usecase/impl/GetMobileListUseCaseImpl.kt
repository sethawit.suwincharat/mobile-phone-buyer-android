package com.full.mobile.domain.usecase.impl

import com.full.mobile.data.remote.base.ApiResponse
import com.full.mobile.domain.models.Mobile
import com.full.mobile.domain.repository.MobilePhoneRepository
import com.full.mobile.domain.usecase.GetMobileListUseCase

class GetMobileListUseCaseImpl(
    private val mobilePhoneRepository: MobilePhoneRepository
) : GetMobileListUseCase {
    override suspend fun execute(sorted: Int): ApiResponse<List<Mobile>> {
        return mobilePhoneRepository.getMobileList(sorted)
    }
}
