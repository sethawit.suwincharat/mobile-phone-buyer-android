package com.full.mobile.data.remote.models


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MobilePhoneResponse(
    @Json(name = "brand")
    val brand: String,
    @Json(name = "description")
    val description: String,
    @Json(name = "id")
    val id: Int,
    @Json(name = "name")
    val name: String,
    @Json(name = "price")
    val price: Double,
    @Json(name = "rating")
    val rating: Double,
    @Json(name = "thumbImageURL")
    val thumbImageURL: String
)
