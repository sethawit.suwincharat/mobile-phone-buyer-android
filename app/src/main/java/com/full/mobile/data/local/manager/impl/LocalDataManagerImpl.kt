package com.full.mobile.data.local.manager.impl

import com.full.mobile.data.local.db.entity.MobileImageEntity
import com.full.mobile.data.local.db.entity.MobilePhoneEntity
import com.full.mobile.data.local.manager.DatabaseManager
import com.full.mobile.data.local.manager.LocalDataManager
import com.full.mobile.data.local.manager.SharedPrefManager

class LocalDataManagerImpl(
    private val databaseManager: DatabaseManager,
    private val sharedPrefManager: SharedPrefManager
) : LocalDataManager {
    override var sorted: Int = sharedPrefManager.sorted

    override suspend fun getString(key: String, defaultValue: String): String {
        return sharedPrefManager.getString(key, defaultValue)
    }

    override suspend fun putString(key: String, value: String) {
        sharedPrefManager.putString(key, value)
    }

    override fun getIntEncryptedSync(key: String): Int {
        return sharedPrefManager.getIntEncryptedSync(key)
    }

    override fun putIntEncryptedSync(key: String, value: Int?): Boolean {
        return sharedPrefManager.putIntEncryptedSync(key, value)
    }

    override suspend fun getLong(key: String, defaultValue: Long): Long {
        return sharedPrefManager.getLong(key, defaultValue)
    }

    override suspend fun putLong(key: String, value: Long) {
        sharedPrefManager.putLong(key, value)
    }

    override suspend fun remove(key: String) {
        sharedPrefManager.remove(key)
    }

    override suspend fun getEncryptedString(key: String, defaultValue: String): String {
        return sharedPrefManager.getEncryptedString(key, defaultValue)
    }

    override suspend fun putEncryptedString(key: String, value: String) {
        sharedPrefManager.putEncryptedString(key, value)
    }

    override fun getEncryptedStringSync(key: String, defaultValue: String): String {
        return sharedPrefManager.getEncryptedStringSync(key, defaultValue)
    }

    override fun getStringSync(key: String, defaultValue: String): String {
        return sharedPrefManager.getStringSync(key, defaultValue)
    }

    override suspend fun getMobileList(sorted: Int): List<MobilePhoneEntity> {
        return databaseManager.getMobileList(sorted)
    }

    override suspend fun saveMobileList(mobilePhoneList: List<MobilePhoneEntity>) {
        databaseManager.saveMobileList(mobilePhoneList)
    }

    override suspend fun getMobile(mobileId: Int): MobilePhoneEntity {
        return databaseManager.getMobile(mobileId)
    }

    override suspend fun updateFavourite(mobileId: Int, isFavourite: Boolean) {
        databaseManager.updateFavourite(mobileId, isFavourite)
    }

    override suspend fun getFavouriteList(sorted: Int): List<MobilePhoneEntity> {
        return databaseManager.getFavouriteList(sorted)
    }

    override suspend fun getMobileImages(mobileId: Int): List<MobileImageEntity> {
        return databaseManager.getMobileImages(mobileId)
    }

    override suspend fun saveMobileImages(mobileImages: List<MobileImageEntity>) {
        databaseManager.saveMobileImages(mobileImages)
    }

}
