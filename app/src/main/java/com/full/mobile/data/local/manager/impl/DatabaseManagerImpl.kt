package com.full.mobile.data.local.manager.impl

import com.full.mobile.data.local.db.MobileDatabase
import com.full.mobile.data.local.db.entity.MobileImageEntity
import com.full.mobile.data.local.db.entity.MobilePhoneEntity
import com.full.mobile.data.local.manager.DatabaseManager

class DatabaseManagerImpl(
    private val database: MobileDatabase
) : DatabaseManager {
    override suspend fun getMobileList(sorted: Int): List<MobilePhoneEntity> {
        return when (sorted) {
            1 -> database.mobilePhoneDao().getMobileListPriceLowToHigh()
            2 -> database.mobilePhoneDao().getMobileListPriceHighToLow()
            3 -> database.mobilePhoneDao().getMobileListRating()
            else -> database.mobilePhoneDao().getMobileListPriceLowToHigh()
        }
    }

    override suspend fun saveMobileList(mobilePhoneList: List<MobilePhoneEntity>) {
        database.mobilePhoneDao().insertIgnore(mobilePhoneList)
    }

    override suspend fun getMobile(mobileId: Int): MobilePhoneEntity {
        return database.mobilePhoneDao().getMobile(mobileId)
    }

    override suspend fun updateFavourite(mobileId: Int, isFavourite: Boolean) {
        database.mobilePhoneDao().updateFavourite(mobileId, isFavourite)
    }

    override suspend fun getFavouriteList(sorted: Int): List<MobilePhoneEntity> {
        return when (sorted) {
            1 -> database.mobilePhoneDao().getFavouriteListPriceLowToHigh()
            2 -> database.mobilePhoneDao().getFavouriteListPriceHighToLow()
            3 -> database.mobilePhoneDao().getFavouriteListRating()
            else -> database.mobilePhoneDao().getFavouriteListPriceLowToHigh()
        }
    }

    override suspend fun getMobileImages(mobileId: Int): List<MobileImageEntity> {
        return database.mobileImageDao().getMobileImages(mobileId)
    }

    override suspend fun saveMobileImages(mobileImages: List<MobileImageEntity>) {
        database.mobileImageDao().insertReplace(mobileImages)
    }
}
