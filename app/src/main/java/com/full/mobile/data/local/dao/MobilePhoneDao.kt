package com.full.mobile.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.full.mobile.data.local.db.entity.MobilePhoneEntity

@Dao
interface MobilePhoneDao : BaseDao<MobilePhoneEntity> {
    @Query("SELECT * FROM MobilePhoneEntity ORDER BY price ASC")
    suspend fun getMobileListPriceLowToHigh(): List<MobilePhoneEntity>

    @Query("SELECT * FROM MobilePhoneEntity ORDER BY price DESC")
    suspend fun getMobileListPriceHighToLow(): List<MobilePhoneEntity>

    @Query("SELECT * FROM MobilePhoneEntity ORDER BY rating DESC")
    suspend fun getMobileListRating(): List<MobilePhoneEntity>

    @Query("SELECT * FROM MobilePhoneEntity WHERE id LIKE :mobileId")
    suspend fun getMobile(mobileId: Int): MobilePhoneEntity

    @Query("UPDATE MobilePhoneEntity SET isFavourite=:isFavourite WHERE id=:mobileId")
    suspend fun updateFavourite(mobileId: Int, isFavourite: Boolean)

    @Query("SELECT * FROM MobilePhoneEntity WHERE isFavourite=1 ORDER BY price ASC")
    suspend fun getFavouriteListPriceLowToHigh(): List<MobilePhoneEntity>

    @Query("SELECT * FROM MobilePhoneEntity WHERE isFavourite=1 ORDER BY price DESC")
    suspend fun getFavouriteListPriceHighToLow(): List<MobilePhoneEntity>

    @Query("SELECT * FROM MobilePhoneEntity WHERE isFavourite=1 ORDER BY rating DESC")
    suspend fun getFavouriteListRating(): List<MobilePhoneEntity>
}
