package com.full.mobile.data.remote.base

import com.full.mobile.data.remote.errors.ApiException

sealed class ApiResponse<out R> {
    data class Success<out T>(val data: T) : ApiResponse<T>()
    data class Error(val apiException: ApiException) : ApiResponse<Nothing>()
    object NetworkError : ApiResponse<Nothing>()
}
