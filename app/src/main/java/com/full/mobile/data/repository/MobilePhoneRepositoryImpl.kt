package com.full.mobile.data.repository

import com.full.mobile.data.local.db.entity.MobileImageEntity
import com.full.mobile.data.local.manager.LocalDataManager
import com.full.mobile.data.mapper.toEntity
import com.full.mobile.data.remote.base.ApiResponse
import com.full.mobile.data.remote.base.NETWORK_ERROR_STATUS
import com.full.mobile.data.remote.base.SOMETHING_WENT_WRONG
import com.full.mobile.data.remote.errors.ApiException
import com.full.mobile.data.remote.errors.ApiExceptionBody
import com.full.mobile.data.remote.service.MobilePhoneService
import com.full.mobile.domain.mapper.toModel
import com.full.mobile.domain.models.Mobile
import com.full.mobile.domain.repository.MobilePhoneRepository

class MobilePhoneRepositoryImpl(
    private val mobilePhoneService: MobilePhoneService,
    private val localDataManager: LocalDataManager
) : MobilePhoneRepository {
    override suspend fun getMobileList(sorted: Int): ApiResponse<List<Mobile>> {
        return when (val networkData =
            request { mobilePhoneService.getMobilePhoneList().map { it.toEntity() } }
        ) {
            is ApiResponse.Success -> {
                localDataManager.saveMobileList(networkData.data)
                val localData = localDataManager.getMobileList(sorted)
                if (!localData.isNullOrEmpty()) request { localData.map { it.toModel() } }
                else ApiResponse.Error(
                    ApiException(
                        ApiExceptionBody(null, SOMETHING_WENT_WRONG)
                    )
                )
            }
            is ApiResponse.Error -> {
                val localData = localDataManager.getMobileList(sorted)
                if (!localData.isNullOrEmpty()) request { localData.map { it.toModel() } }
                else ApiResponse.Error(
                    ApiException(
                        ApiExceptionBody(null, SOMETHING_WENT_WRONG)
                    )
                )
            }
            is ApiResponse.NetworkError -> {
                val localData = localDataManager.getMobileList(sorted)
                request { localData.map { it.toModel() } }
                if (!localData.isNullOrEmpty()) request { localData.map { it.toModel() } }
                else ApiResponse.Error(
                    ApiException(
                        ApiExceptionBody(null, NETWORK_ERROR_STATUS)
                    )
                )
            }
        }
    }

    override suspend fun setFavourite(mobileId: Int) {
        val data = localDataManager.getMobile(mobileId)
        localDataManager.updateFavourite(
            data.id,
            !data.isFavourite
        )
    }

    override suspend fun getFavouriteList(sorted: Int): List<Mobile> {
        return localDataManager.getFavouriteList(sorted).map { it.toModel() }
    }

    override suspend fun getMobileImages(mobileId: Int): ApiResponse<List<MobileImageEntity>> {
        return when (val networkData =
            request { mobilePhoneService.getMobilePhoneImagesById(mobileId).map { it.toEntity() } }
        ) {
            is ApiResponse.Success -> {
                localDataManager.saveMobileImages(networkData.data)
                val localData = localDataManager.getMobileImages(mobileId)
                if (!localData.isNullOrEmpty()) request { localData }
                else ApiResponse.Error(
                    ApiException(
                        ApiExceptionBody(null, SOMETHING_WENT_WRONG)
                    )
                )
            }
            is ApiResponse.Error -> {
                val localData = localDataManager.getMobileImages(mobileId)
                if (!localData.isNullOrEmpty()) request { localData }
                else ApiResponse.Error(
                    ApiException(
                        ApiExceptionBody(null, SOMETHING_WENT_WRONG)
                    )
                )
            }
            is ApiResponse.NetworkError -> {
                val localData = localDataManager.getMobileImages(mobileId)
                if (!localData.isNullOrEmpty()) request { localData }
                else ApiResponse.Error(
                    ApiException(
                        ApiExceptionBody(null, NETWORK_ERROR_STATUS)
                    )
                )
            }
        }
    }
}
