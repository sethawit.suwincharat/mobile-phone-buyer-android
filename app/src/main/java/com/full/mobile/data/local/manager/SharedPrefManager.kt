package com.full.mobile.data.local.manager

import com.full.mobile.Constants.DEFAULT_LONG
import com.full.mobile.Constants.EMPTY_STRING

interface SharedPrefManager {
    var sorted: Int
    suspend fun getString(key: String, defaultValue: String = EMPTY_STRING): String
    suspend fun putString(key: String, value: String)
    fun getIntEncryptedSync(key: String): Int
    fun putIntEncryptedSync(key: String, value: Int?): Boolean
    suspend fun getLong(key: String, defaultValue: Long = DEFAULT_LONG): Long
    suspend fun putLong(key: String, value: Long)
    suspend fun remove(key: String)
    suspend fun getEncryptedString(key: String, defaultValue: String = EMPTY_STRING): String
    suspend fun putEncryptedString(key: String, value: String)
    fun getEncryptedStringSync(key: String, defaultValue: String = EMPTY_STRING): String
    fun getStringSync(key: String, defaultValue: String = EMPTY_STRING): String
}
