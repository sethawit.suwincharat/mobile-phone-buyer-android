package com.full.mobile.data.local.manager

object SharedPrefConstant {
    const val SHARED_PREF_NAME = "Project_Pref_Name"
    const val ENCRYPTED_SHARED_PREF_NAME = "Project_Encrypted_Pref_Name"
}
