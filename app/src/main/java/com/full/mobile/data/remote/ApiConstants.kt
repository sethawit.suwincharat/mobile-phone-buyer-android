package com.full.mobile.data.remote

object StatusCode {
    const val INTERNAL_ERROR = 105
}

const val EXPIRE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSX"

fun getApiTag(url: String, baseUrl: String): String {
    return url.removePrefix(baseUrl)
}
