package com.full.mobile.data.remote.errors

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.IOException

@JsonClass(generateAdapter = true)
data class ApiExceptionBody(
    @Json(name = "code") val errorCode: String?,
    @Json(name = "message") val errorMessage: String
)

data class ApiException(
    val error: ApiExceptionBody?
) : IOException("${error?.errorCode} ${error?.errorMessage}")
