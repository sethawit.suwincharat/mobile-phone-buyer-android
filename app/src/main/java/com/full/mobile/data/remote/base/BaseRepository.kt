package com.full.mobile.data.remote.base

import com.full.mobile.data.remote.errors.ApiException
import com.full.mobile.data.remote.errors.ApiExceptionBody
import com.squareup.moshi.Moshi
import retrofit2.HttpException

const val NETWORK_ERROR_STATUS = "No Internet Connection"
const val SOMETHING_WENT_WRONG = "Something Went Wrong"

interface BaseRepository {
    suspend fun <T> request(call: suspend () -> T): ApiResponse<T> {
        return try {
            ApiResponse.Success(call())
        } catch (apiException: ApiException) {
            when (apiException.error?.errorMessage) {
                NETWORK_ERROR_STATUS -> {
                    ApiResponse.NetworkError
                }
                else -> {
                    ApiResponse.Error(apiException)
                }
            }
        }
    }

    private fun convertErrorBody(throwable: HttpException): ApiExceptionBody? {
        return try {
            throwable.response()?.errorBody()?.source()?.let {
                val moshiAdapter = Moshi.Builder().build().adapter(ApiExceptionBody::class.java)
                moshiAdapter.fromJson(it)
            }
        } catch (exception: Exception) {
            null
        }
    }
}
