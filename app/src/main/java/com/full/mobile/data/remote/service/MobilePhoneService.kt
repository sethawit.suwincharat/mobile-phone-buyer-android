package com.full.mobile.data.remote.service

import com.full.mobile.data.remote.models.MobilePhoneDetailResponse
import com.full.mobile.data.remote.models.MobilePhoneResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface MobilePhoneService {
    @GET("/api/mobiles")
    suspend fun getMobilePhoneList(): List<MobilePhoneResponse>

    @GET("/api/mobiles/{mobile_id}/images")
    suspend fun getMobilePhoneImagesById(
        @Path("mobile_id") mobileId: Int
    ): List<MobilePhoneDetailResponse>
}
