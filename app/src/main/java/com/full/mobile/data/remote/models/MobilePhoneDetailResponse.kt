package com.full.mobile.data.remote.models


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MobilePhoneDetailResponse(
    @Json(name = "id")
    val id: Int,
    @Json(name = "mobile_id")
    val mobileId: Int,
    @Json(name = "url")
    val url: String
)
