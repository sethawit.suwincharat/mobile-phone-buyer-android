package com.full.mobile.data.mapper

import com.full.mobile.data.local.db.entity.MobileImageEntity
import com.full.mobile.data.local.db.entity.MobilePhoneEntity
import com.full.mobile.data.remote.models.MobilePhoneDetailResponse
import com.full.mobile.data.remote.models.MobilePhoneResponse

fun MobilePhoneResponse.toEntity() = MobilePhoneEntity(
    id = id,
    name = name,
    brand = brand,
    description = description,
    price = price,
    rating = rating,
    thumbImageURL = thumbImageURL
)

fun MobilePhoneDetailResponse.toEntity() = MobileImageEntity(
    id = id,
    mobileId = mobileId,
    url = url
)
