package com.full.mobile.data.local.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class MobileImageEntity(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,
    val mobileId: Int,
    val url: String
)
