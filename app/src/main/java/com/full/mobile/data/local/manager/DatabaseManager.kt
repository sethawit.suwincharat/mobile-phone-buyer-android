package com.full.mobile.data.local.manager

import com.full.mobile.data.local.db.entity.MobileImageEntity
import com.full.mobile.data.local.db.entity.MobilePhoneEntity

interface DatabaseManager {
    suspend fun getMobileList(sorted: Int): List<MobilePhoneEntity>
    suspend fun saveMobileList(mobilePhoneList: List<MobilePhoneEntity>)
    suspend fun getMobile(mobileId: Int): MobilePhoneEntity
    suspend fun updateFavourite(mobileId: Int, isFavourite: Boolean)
    suspend fun getFavouriteList(sorted: Int): List<MobilePhoneEntity>
    suspend fun getMobileImages(mobileId: Int): List<MobileImageEntity>
    suspend fun saveMobileImages(mobileImages: List<MobileImageEntity>)
}
