package com.full.mobile.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.full.mobile.data.local.db.entity.MobileImageEntity

@Dao
interface MobileImageDao : BaseDao<MobileImageEntity> {
    @Query("SELECT * FROM MobileImageEntity WHERE mobileId=:mobileId")
    suspend fun getMobileImages(mobileId: Int): List<MobileImageEntity>
}
