package com.full.mobile.data.local.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class MobilePhoneEntity(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,
    val name: String,
    val brand: String,
    val description: String,
    val price: Double,
    val rating: Double,
    val thumbImageURL: String,
    val isFavourite: Boolean = false
)
