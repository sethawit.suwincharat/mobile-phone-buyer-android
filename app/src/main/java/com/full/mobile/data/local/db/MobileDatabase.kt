package com.full.mobile.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.full.mobile.data.local.dao.MobileImageDao
import com.full.mobile.data.local.dao.MobilePhoneDao
import com.full.mobile.data.local.db.entity.MobileImageEntity
import com.full.mobile.data.local.db.entity.MobilePhoneEntity

@Database(
    entities = [MobilePhoneEntity::class, MobileImageEntity::class],
    version = 1,
    exportSchema = false
)
abstract class MobileDatabase : RoomDatabase() {
    abstract fun mobilePhoneDao(): MobilePhoneDao
    abstract fun mobileImageDao(): MobileImageDao
}
