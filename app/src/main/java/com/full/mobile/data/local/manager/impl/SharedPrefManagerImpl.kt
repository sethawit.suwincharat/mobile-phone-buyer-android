package com.full.mobile.data.local.manager.impl

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.security.keystore.KeyProperties.PURPOSE_DECRYPT
import android.security.keystore.KeyProperties.PURPOSE_ENCRYPT
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV
import androidx.security.crypto.EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
import androidx.security.crypto.MasterKey
import androidx.security.crypto.MasterKey.DEFAULT_AES_GCM_MASTER_KEY_SIZE
import androidx.security.crypto.MasterKey.DEFAULT_MASTER_KEY_ALIAS
import com.full.mobile.data.local.manager.SharedPrefConstant.ENCRYPTED_SHARED_PREF_NAME
import com.full.mobile.data.local.manager.SharedPrefConstant.SHARED_PREF_NAME
import com.full.mobile.data.local.manager.SharedPrefManager

class SharedPrefManagerImpl(private val context: Context) : SharedPrefManager {
    private val sharedPrefName by lazy { SHARED_PREF_NAME }
    private val encryptedSharedPrefName by lazy { ENCRYPTED_SHARED_PREF_NAME }
    private val key by lazy {
        MasterKey.Builder(context)
            .setKeyGenParameterSpec(getSpec())
            .build()
        /**
         * For SDK <= M
         * MasterKey.Builder(context).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build()
         * */
    }
    private val encryptedPreferences by lazy {
        EncryptedSharedPreferences.create(
            context,
            encryptedSharedPrefName,
            key,
            AES256_SIV,
            AES256_GCM
        )
    }
    private val sharedPreferences by lazy {
        context.getSharedPreferences(sharedPrefName, MODE_PRIVATE)
    }
    private val encryptedEditor by lazy { encryptedPreferences.edit() }
    private val editor by lazy { sharedPreferences.edit() }

    private fun getSpec(): KeyGenParameterSpec {
        return KeyGenParameterSpec.Builder(
            DEFAULT_MASTER_KEY_ALIAS,
            PURPOSE_ENCRYPT or PURPOSE_DECRYPT
        )
            .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
            .setKeySize(DEFAULT_AES_GCM_MASTER_KEY_SIZE)
            .build()
    }

    override var sorted: Int
        get() = getIntEncryptedSync("sorted")
        set(value) {
            putIntEncryptedSync("sorted", value)
        }

    override suspend fun getString(key: String, defaultValue: String): String {
        return sharedPreferences.getString(key, defaultValue) ?: defaultValue
    }

    override suspend fun putString(key: String, value: String) {
        editor.putString(key, value).commit()
    }

    override fun getIntEncryptedSync(key: String): Int =
        encryptedPreferences.getInt(key, 1)

    override fun putIntEncryptedSync(key: String, value: Int?): Boolean {
        val editor = encryptedPreferences.edit()
        if (value != null) {
            editor.putInt(key, value)
        } else {
            editor.remove(key)
        }
        return editor.commit()
    }

    override suspend fun getLong(key: String, defaultValue: Long): Long {
        return sharedPreferences.getLong(key, defaultValue)
    }

    override suspend fun putLong(key: String, value: Long) {
        editor.putLong(key, value).commit()
    }

    override suspend fun remove(key: String) {
        editor.remove(key).commit()
    }

    override suspend fun getEncryptedString(key: String, defaultValue: String): String {
        return encryptedPreferences.getString(key, defaultValue) ?: defaultValue
    }

    override suspend fun putEncryptedString(key: String, value: String) {
        encryptedEditor.putString(key, value)
    }

    override fun getEncryptedStringSync(key: String, defaultValue: String): String {
        return encryptedPreferences.getString(key, defaultValue) ?: defaultValue
    }

    override fun getStringSync(key: String, defaultValue: String): String {
        return sharedPreferences.getString(key, defaultValue) ?: defaultValue
    }
}
