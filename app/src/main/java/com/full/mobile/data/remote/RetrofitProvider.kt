package com.full.mobile.data.remote

import com.full.mobile.BuildConfig
import com.full.mobile.data.network.NetworkStateProvider
import com.full.mobile.data.remote.base.NETWORK_ERROR_STATUS
import com.full.mobile.data.remote.errors.ApiException
import com.full.mobile.data.remote.errors.ApiExceptionBody
import com.squareup.moshi.Moshi
import java.io.IOException
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


class RetrofitProvider(
    private val moshi: Moshi,
    private val networkStateProvider: NetworkStateProvider,
    private val apiUrl: String
) {
    fun provide(): Retrofit {
        val client = OkHttpClient.Builder()
            .setConnectivityHandler()
            .handleErrors()
            .setLogger(HttpLoggingInterceptor.Level.BODY)
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .build()

        return Retrofit.Builder()
            .client(client)
            .setConverters()
            .baseUrl(apiUrl)
            .build()
    }

    private fun Retrofit.Builder.setConverters() =
        addConverterFactory(MoshiConverterFactory.create(moshi))

    private fun OkHttpClient.Builder.setLogger(
        logLevel: HttpLoggingInterceptor.Level
    ): OkHttpClient.Builder {
        if (BuildConfig.DEBUG) {
            addInterceptor(HttpLoggingInterceptor().apply {
                level = logLevel
            })
        }
        return this
    }

    private fun OkHttpClient.Builder.setConnectivityHandler() = addInterceptor { chain ->
        if (networkStateProvider.isOnline()) {
            chain.proceed(chain.request())
        } else {
            throw ApiException(ApiExceptionBody("error", NETWORK_ERROR_STATUS))
        }
    }

    private fun OkHttpClient.Builder.handleErrors() = addInterceptor { chain ->
        val response = chain.proceed(chain.request())
        if (response.isSuccessful) {
            response
        } else {
            val code = response.code
            val body = response.body
            throw if (body == null) {
                ApiException(ApiExceptionBody(code.toString(), "Server response is empty"))
            } else {
                try {
                    val error = moshi.adapter(ApiExceptionBody::class.java).fromJson(body.string())
                    val errorUpdate =
                        if (error?.errorCode == "error")
                            error.copy(errorCode = code.toString())
                        else error
                    ApiException(errorUpdate)
                } catch (ex: IOException) {
                    ApiException(ApiExceptionBody(code.toString(), NETWORK_ERROR_STATUS))
                } catch (ex: Exception) {
                    ApiException(
                        ApiExceptionBody(
                            code.toString(),
                            "Cannot parse error: ${ex.message}"
                        )
                    )
                } finally {
                    body.close()
                }
            }
        }
    }
}
