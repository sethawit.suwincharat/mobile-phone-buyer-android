package com.full.mobile.data.network

import android.content.Context

interface NetworkStateProvider {
    val context: Context
    fun isOnline(): Boolean
}

class NetworkStateProviderImpl(override val context: Context) : NetworkStateProvider {
    override fun isOnline(): Boolean {
        val manager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE)
                    as? android.net.ConnectivityManager
        val capabilities = manager?.getNetworkCapabilities(manager.activeNetwork) ?: return false
        return capabilities.hasTransport(android.net.NetworkCapabilities.TRANSPORT_WIFI) ||
                capabilities.hasTransport(android.net.NetworkCapabilities.TRANSPORT_CELLULAR) ||
                capabilities.hasTransport(android.net.NetworkCapabilities.TRANSPORT_ETHERNET)
    }
}
