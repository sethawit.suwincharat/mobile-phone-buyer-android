package com.full.mobile

import android.app.Application
import androidx.databinding.library.BuildConfig
import com.full.mobile.di.apiModule
import com.full.mobile.di.appModule
import com.full.mobile.di.dataModule
import com.full.mobile.di.repositoryModule
import com.full.mobile.di.useCaseModule
import com.full.mobile.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class MobilePhoneBuyerApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        initTimber()
        initKoin()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initKoin() {
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@MobilePhoneBuyerApplication)
            modules(
                dataModule,
                appModule,
                repositoryModule,
                useCaseModule,
                viewModelModule,
                apiModule,
            )
        }
    }
}

