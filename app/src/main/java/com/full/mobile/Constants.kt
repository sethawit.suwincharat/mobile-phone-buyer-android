package com.full.mobile

object Constants {
    const val EMPTY_STRING = ""
    const val DEFAULT_LONG = 0L
}

object LocaleConstants {
    const val LOCALE_EN = "en"
}
